-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 01, 2015 at 02:31 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `account`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(30) NOT NULL,
  `level` varchar(20) NOT NULL,
  `first_name` varchar(20) DEFAULT NULL,
  `last_name` varchar(20) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `account_status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `level`, `first_name`, `last_name`, `last_login`, `account_status`) VALUES
(1, 'ashik', 'pass', 'admin', 'Ashik', 'Rahaman', NULL, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `entry`
--

CREATE TABLE IF NOT EXISTS `entry` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `time` time NOT NULL,
  `date` date NOT NULL,
  `description` text NOT NULL,
  `received` int(15) DEFAULT NULL,
  `expend` int(15) DEFAULT NULL,
  `remarks` varchar(150) DEFAULT NULL,
  `issued` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `entry`
--

INSERT INTO `entry` (`id`, `time`, `date`, `description`, `received`, `expend`, `remarks`, `issued`) VALUES
(1, '02:59:00', '2015-11-28', 'This is sample entry', 500, 4000, 'This is remarks', 'ashik'),
(2, '02:53:00', '2015-11-28', 'afdsf', 785, 876, 'fdhdsh', 'ashik'),
(3, '02:57:00', '2015-11-27', 'dsafd', 65, 763, '763', 'ashik'),
(4, '02:59:00', '2015-11-26', 'This is ashik''s entry', 50, 5, 'dsaf', 'ashik'),
(5, '03:01:00', '2015-11-26', 'Domain name purchase', 1200, 1000, 'For client', 'ashik'),
(6, '03:01:00', '2015-11-25', 'test', 102, 50, 'test remarks', 'ashik'),
(7, '11:31:00', '2015-12-01', 'Zinnah', 1200, 500, 'This is Ali Zinnah', 'ashik');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
