
<?php 
	function redirect_to($link){
		header("Location: ".$link);
		exit();
	}

	function remove_under_score($str){
		return str_replace("_", " ", $str);
	}

	function mysql_prep($string){
		global $connection;
		return mysqli_real_escape_string($connection,$string);
	}

	function total_amount($startfrom,$endof,$i){
		$query = "SELECT SUM(`{$i}`) FROM entry WHERE `date` ";
        if(isset($startfrom) && isset($endof)){
          $query .= "BETWEEN '{$startfrom}' AND '{$endof}' ORDER BY id DESC ";
        }
        return $query;
	}

	
 ?>