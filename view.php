<?php session_start(); ?>
<?php require_once("includes/db.php"); ?>
<?php require_once("includes/function.php"); ?>
<?php 
    if (!isset($_SESSION["admin"])) {
        redirect_to("admin.php");
    }else{
        //$_SESSION["admin"] = NULL;
    }
 ?>

<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>View All Entry</title>
    <link rel="stylesheet" href="css/reset.css">

    <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
    <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="css/style.css">
  </head>

<body>

    
<!-- Form Mixin-->
<!-- Input Mixin-->
<!-- Button Mixin-->
<!-- Pen Title-->
<div class="pen-title">
  <span><i class='fa fa-paint-brush'></i> Design + <i class='fa fa-code'></i> Code by <a href='#'>Ashik</a></span>
</div>
<!-- Form Module-->
<div class="module form-module view">
  <div class="toggle"><a href="admin.php"><i class="fa fa-sign-out"></i></a>
    <div class="tooltip">Click to Logout</div>
  </div>
  <div class="form">
    <div class="search_box">
    <form action="date_wise.php" method="get">
       VIEW AS &nbsp;
      <select name="start" id="">
      <?php 
        $query = "SELECT DISTINCT `date` FROM entry ORDER BY `date` ASC";
        $result = mysqli_query($connection,$query);
        if ($result) {
            while ($account = mysqli_fetch_assoc($result)) {
                echo "<option value=\"{$account['date']}\">{$account['date']}</option>";
            }
        }
        mysqli_free_result($result);
      ?>  
      </select>

      To

      <select name="end" id="">
      <?php 
        $query = "SELECT DISTINCT `date` FROM entry ORDER BY `date` DESC";
        $result = mysqli_query($connection,$query);
        if ($result) {
            while ($account = mysqli_fetch_assoc($result)) {
                echo "<option value=\"{$account['date']}\">{$account['date']}</option>";
            }
        }
        mysqli_free_result($result);
      ?>  
      </select>

      <input type="submit" name="submit" value="Search">
    </form>
    </div>
    <h2>View All Entry</h2>
    <?php echo $message = isset($_SESSION["message"]) ? $_SESSION["message"] : ""; ?>
    <?php if (isset($_SESSION["message"])) {$_SESSION["message"] = NULL;}?>
    <table class="table">
      <thead>
        <tr>
          <th>SL</th>
          <th>Date</th>
          <th>Description</th>
          <th style="text-align:right;">Received</th>
          <th style="text-align:right;">Expend</th>
          <th>Remarks</th>
          <th>Issued</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php
            $item_per_page = 10;
            $total_item = mysqli_num_rows(mysqli_query($connection,"SELECT * FROM entry"));
            $pages = ceil($total_item/$item_per_page);
            $page = isset($_GET["page"]) ? (int)$_GET["page"] : $_GET["page"] = 1;
            $start = (($page - 1)*$item_per_page);

            $query = "SELECT * FROM entry ORDER BY `date` DESC LIMIT {$start},{$item_per_page}";
            $result = mysqli_query($connection,$query);
         ?>
        <?php 
          if ($result) {
            while ($account = mysqli_fetch_assoc($result)) {
        ?>
        <tr class="">
          <td><?php echo $account["id"] ?></td>
          <td><?php echo $account["date"] ?></td>
          <td><?php echo $account["description"] ?></td>
          <td><?php echo $account["received"] ?></td>
          <td><?php echo $account["expend"] ?></td>
          <td><?php echo $account["remarks"] ?></td>
          <td><?php echo $account["issued"] ?></td>
          <td><a href="edit.php?id=<?php echo $account["id"] ?>"><i class="fa fa-times fa-edit"></i></a> &nbsp <a href="delete.php?id=<?php echo $account["id"] ?>" onClick="return confirm('Are you really want to delete this item?')"><i class='fa fa-trash-o'></i></a></td>
        </tr>
        <?php
              # code...
            }
          }
        ?>
      </tbody>
    </table>
    <div class="page">
      <ul class="pagination"> 
        <li><a href=""><span class="la">&laquo;</span> </a></li>
        <?php 
        for($i=1;$i<=$pages;$i++){
          if($page == $i){
        ?>
          <li><a href="view.php?page=<?php echo $i; ?>"><?php echo "{$i}"; ?></a></li>
        <?php 
          }else{ 
        ?>
          <li><a href="view.php?page=<?php echo $i; ?>"><?php echo "{$i}"; ?></a></li>
        <?php
          }
        }
       ?>
       <li><a href=""><span class="ra">&raquo;</span></a></li>
      </ul>
    </div>
  </div>
  <div class="cta"><a href="add.php">Add new entry</a></div>
</div>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='js/da0415260bc83974687e3f9ae.js'></script>
<script src="js/index.js"></script>

    
    
    
  </body>
</html>
