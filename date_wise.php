<?php session_start(); ?>
<?php require_once("includes/db.php"); ?>
<?php require_once("includes/function.php"); ?>
<?php 
    if (!isset($_SESSION["admin"])) {
        redirect_to("admin.php");
    }elseif(isset($_REQUEST["submit"])){
      $startfrom = $_REQUEST["start"];
      $endof = $_REQUEST["end"];
    }else{
      redirect_to("view.php");
    }
 ?>

<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>View All Entry</title>
    <link rel="stylesheet" href="css/reset.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
    <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="css/style.css">
  </head>

<body>

    
<!-- Form Mixin-->
<!-- Input Mixin-->
<!-- Button Mixin-->
<!-- Pen Title-->
<div class="pen-title">
  <span><i class='fa fa-paint-brush'></i> Design + <i class='fa fa-code'></i> Code by <a href='#'>Ashik</a></span>
</div>
<!-- Form Module-->
<div class="module form-module view">
  <div class="toggle"><a href="admin.php"><i class="fa fa-sign-out"></i></a>
    <div class="tooltip">Click to Logout</div>
  </div>
  <div class="form">
    <div class="search_box">
    <form action="date_wise.php" method="get">
       VIEW AS &nbsp;
      <select name="start" id="">
      <?php 
        $query = "SELECT DISTINCT `date` FROM entry ORDER BY `date` ASC";
        $result = mysqli_query($connection,$query);
        if ($result) {
            while ($account = mysqli_fetch_assoc($result)) {
                echo "<option value=\"{$account['date']}\">{$account['date']}</option>";
            }
        }
        mysqli_free_result($result);
      ?>  
      </select>

      To

      <select name="end" id="">
      <?php 
        $query = "SELECT DISTINCT `date` FROM entry ORDER BY `date` DESC";
        $result = mysqli_query($connection,$query);
        if ($result) {
            while ($account = mysqli_fetch_assoc($result)) {
                echo "<option value=\"{$account['date']}\">{$account['date']}</option>";
            }
        }
        mysqli_free_result($result);
      ?>  
      </select>

      <input type="submit" name="submit" value="Search">
    </form>
    </div>
    <h2>View All Entry Date Wise</h2>
    <?php echo $message = isset($_SESSION["message"]) ? $_SESSION["message"] : ""; ?>
    <?php if (isset($_SESSION["message"])) {$_SESSION["message"] = NULL;}?>
    <div id="print">
    <table class="table">
      <thead>
        <tr>
          <th>SL</th>
          <th>Date</th>
          <th>Description</th>
          <th style="text-align:right;">Received</th>
          <th style="text-align:right;">Expend</th>
          <th>Remarks</th>
          <th>Issued</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php
            // $item_per_page = 10;
            // $query = "SELECT * FROM entry ";
            // if(isset($startfrom) && isset($endof)){
            //   $query .= "WHERE `date` BETWEEN '{$startfrom}' AND '{$endof}' ";
            // }
            // $result = mysqli_query($connection,$query);
            // $total_item = mysqli_num_rows($result);
            // $pages = ceil($total_item/$item_per_page);
            // $page = isset($_GET["page"]) ? (int)$_GET["page"] : $_GET["page"] = 1;
            // $start = (($page - 1)*$item_per_page);

            $query = "SELECT * FROM entry WHERE `date` ";
            if(isset($startfrom) && isset($endof)){
              $query .= "BETWEEN '{$startfrom}' AND '{$endof}' ORDER BY `date` DESC ";
            }
            $result = mysqli_query($connection,$query);
         ?>
        <?php 
          if ($result) {
            while ($account = mysqli_fetch_assoc($result)) {
        ?>
        <tr class="">
          <td><?php echo $account["id"] ?></td>
          <td><?php echo $account["date"] ?></td>
          <td><?php echo $account["description"] ?></td>
          <td style="text-align:right;"><?php echo $account["received"] ?></td>
          <td style="text-align:right;"><?php echo $account["expend"] ?></td>
          <td><?php echo $account["remarks"] ?></td>
          <td><?php echo $account["issued"] ?></td>
          <td><a href="edit.php?id=<?php echo $account["id"] ?>"><i class="fa fa-times fa-edit"></i></a> &nbsp; <a href="delete.php?id=<?php echo $account["id"] ?>"><i class='fa fa-trash-o'></i></a></td>
        </tr>
        <?php
              # code...
            }
          }
          mysqli_free_result($result);
        ?>

        <?php 
            $query = total_amount($startfrom,$endof,"received");
            $result = mysqli_query($connection,$query);
            $total_received = mysqli_fetch_array($result);
         ?>
        <tr>
          <td>&nbsp;</td>
          <td colspan="2" style="text-align:right;border-right:1px solid #33B5E5;">Total</td>
          <td style="text-align:right;border-right:1px solid #33B5E5;"><?php echo $total_received["0"]; ?></td>
          <?php
            $query = total_amount($startfrom,$endof,"expend");
            $result = mysqli_query($connection,$query);
            $total_expend = mysqli_fetch_array($result);
         ?>
          <td style="text-align:right;border-right:1px solid #33B5E5;"><?php echo $total_expend["0"]; ?></td>
          <td style="text-align:right;border-right:1px solid #33B5E5;">
            <?php 
              if($total_received["0"] > $total_expend["0"]){
                echo "Total Profit";
              }elseif($total_received["0"] < $total_expend["0"]){
                echo "Total Loss";
              }
             ?>
          </td>
          <td style="text-align:right;border-right:1px solid #33B5E5;">
            <?php 
            if($total_received["0"] > $total_expend["0"]){
              echo $total_received["0"]-$total_expend["0"];
            }elseif($total_received["0"] < $total_expend["0"]){
              echo $total_expend["0"]-$total_received["0"];
            }
           ?>
          </td>
          <td>
              <button type="button" onclick="printDiv('print')" class="print_button" style="padding:2px 5px;margin:0px;">Print</button>
            <script>
              function printDiv(divName) {
                 var printContents = document.getElementById(divName).innerHTML;
                 var originalContents = document.body.innerHTML;

                 document.body.innerHTML = printContents;

                 window.print();

                 document.body.innerHTML = originalContents;
            }
        </script>
          </td>
        </tr>
      </tbody>
    </table>
    </div><!-- End Print Div -->
  </div>
  <div class="cta"><a href="view.php">View all entry</a></div>
</div>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='js/da0415260bc83974687e3f9ae.js'></script>

        <script src="js/index.js"></script>

    
    
    
  </body>
</html>
