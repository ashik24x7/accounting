<?php session_start(); ?>
<?php require_once("includes/db.php"); ?>
<?php require_once("includes/function.php"); ?>
<?php 
    if (!isset($_SESSION["admin"])) {
        redirect_to("admin.php");
    }else{
        //$_SESSION["admin"] = NULL;
    }

 ?>
<?php 
    if (isset($_REQUEST["submit"])) {
        $fields = array(
            "description",
            "received",
            "expend",
            "remarks"
            );
        
        $errors = array();
        foreach($fields as $field){
            if (empty($_REQUEST[$field])) {
                $errors[$field] = "<p class=\"error\">* ".ucfirst(remove_under_score($field)." is empty</p>");
             }
        }

        if (empty($errors)) {
            date_default_timezone_set("Asia/Dhaka");
            $time = strftime("%I:%M",time());
            $date =date("Y-m-d");
            $description = mysql_prep($_REQUEST['description']);
            $remarks = mysql_prep($_REQUEST['remarks']);
            $query = "INSERT INTO entry( ";
            $query .= "`time`, ";
            $query .= "`date`, ";
            $query .= "`description`, ";
            $query .= "`received`, ";
            $query .= "`expend`, ";
            $query .= "`remarks`, ";
            $query .= "`issued` ";
            $query .= ") VALUES ( "; 
            $query .= "'{$time}', ";
            $query .= "'{$date}', ";
            $query .= "'{$description}', ";
            $query .= "'{$_REQUEST['received']}', ";
            $query .= "'{$_REQUEST['expend']}', ";
            $query .= "'{$remarks}', ";
            $query .= "'{$_REQUEST['issued']}' ";
            $query .=")";

            $result = mysqli_query($connection,$query);

            if ($result) {
                $_SESSION["message"] = "<p class=\"message_success\">New entry has added sucessfully</P>";
                redirect_to("view.php");
            }else{
                $message = "<p class=\"message_error\">New entry addition failed!</P>";
            }

        }else{
            $message = "<p class=\"message_error\">There were some ERROR!</P>";
        }
        

    }else{

    }

 ?>
<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Add New Entry</title>

    <link rel="stylesheet" href="css/reset.css">
    <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
    <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="css/style.css">
  </head>


  <body>
  <!-- Form Mixin-->
  <!-- Input Mixin-->
  <!-- Button Mixin-->
  <!-- Pen Title-->
    <div class="pen-title">
      <span><i class='fa fa-paint-brush'></i> Design + <i class='fa fa-code'></i> Code by <a href='#'>Ashik</a></span>
    </div>
    <!-- Form Module-->
    <div class="module form-module">
      <div class="toggle"><a href="admin.php"><i class="fa fa-sign-out"></i></a>
        <div class="tooltip">Click to Logout</div>
      </div>
      <div class="form">
        <h2>Add New Entry</h2>
        <?php echo $message = isset($message) ? $message : ""; ?>
        <form action="add.php" method="post">

          <?php if(!empty($errors["description"])){echo $errors["description"];} ?>
          <textarea name="description" id="" cols="30" rows="8" placeholder="Description"><?php if(isset($_POST['description'])){echo $_POST['description'];} ?></textarea>

          <?php if(!empty($errors["received"])){echo $errors["received"];} ?>
          <input type="number" name="received" placeholder="Received" value="<?php if(isset($_POST['received'])){echo $_POST['received'];} ?>"/>

          <?php if(!empty($errors["expend"])){echo $errors["expend"];} ?>
          <input type="number" name="expend" placeholder="Expend" value="<?php if(isset($_POST['expend'])){echo $_POST['expend'];} ?>"/>

          <?php if(!empty($errors["remarks"])){echo $errors["remarks"];} ?>
          <input type="text" name="remarks" placeholder="Remarks" value="<?php if(isset($_POST['remarks'])){echo $_POST['remarks'];} ?>"/>

          <input type="hidden" name="issued" placeholder="Remarks" value="<?php if(isset($_SESSION["admin"])){echo $_SESSION["admin"];} ?>"/>
          <button name="submit" value="submit">Add an Entry</button>
        </form>
      </div>
      <div class="cta"><a href="view.php">View all account history</a></div>
    </div>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src='js/da0415260bc83974687e3f9ae.js'></script>
    <script src="js/index.js"></script>
  </body>
</html>
